package com.mybatis.jpa.model.type;

/***
 *
 * @author svili
 *
 */
public enum SexEnum {

    UNKNOW, MALE, FEMALE;

}
